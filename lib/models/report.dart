class Report {
  String id;
  String contractNumber;
  String description;
  String jobName;
  String userFullName;
  int duration;
  int userId;
  double lat;
  double lng;
  List<ImageData> images;

  Report({this.id, this.contractNumber, this.description, this.jobName, this.duration, this.lat, this.lng, this.images, this.userId, this.userFullName});

  factory Report.fromMap(Map<String, dynamic> data) => 
    Report(
      id: data['id'],
      contractNumber: data['contractNumber'],
      description: data['description'],
      jobName: data['jobName'],
      duration: data['duration'],
      lat: data['lat'],
      lng: data['lng'],
      images: data['images']
        .map<ImageData>((item) => ImageData.fromMap(item))
        .toList(),
      userId: data['user']['id'],
      userFullName: data['user']['personal']['fullName'] == null
        ? data['user']['personal']['fullName']
        : data['user']['username']
    );
}

class ImageData {
  String fileName;
  String mimeType;
  String url;

  ImageData({this.fileName, this.mimeType, this.url});

  factory ImageData.fromMap(Map<String, dynamic> data) => 
    ImageData(
      fileName: data['fileName'],
      mimeType: data['mimeType'],
      url: data['url']
    );
}