import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:graphql/client.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mots_admin/gql/client.dart';
import 'package:mots_admin/screens/home/home.screen.dart';
import 'package:mots_admin/screens/login/login.screen.dart';
import 'package:mots_admin/screens/report_detail/report-detail.dart';
import 'package:mots_admin/screens/splashscreen/splashscreen.screen.dart';
import 'package:mots_admin/theme/style.dart';

import 'blocs/auth/bloc.dart';
import 'blocs/report/bloc.dart';

Future main() async {
  await DotEnv().load('.env');
  runApp(MotsAdmin());
}

class MotsAdmin extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    ValueNotifier<GraphQLClient> gplClientVn = ValueNotifier(gqlClient);

    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          builder: (context) => AuthBloc(gqlClient),
        ),
        BlocProvider<ReportBloc>(
          builder: (context) => ReportBloc(gqlClient),
        ),
      ],
      child: GraphQLProvider(
        client: gplClientVn,
        child: MaterialApp(
          title: 'Mots Admin',
          theme: appTheme(),
          initialRoute: '/',
          routes: <String, WidgetBuilder>{
            '/': (BuildContext context) => SplashScreen(),
            '/login': (BuildContext context) => LoginScreen(),
            '/dashboard': (BuildContext context) => HomeScreen(),
            '/report-detail': (BuildContext context) => ReportDetailScreen(),
          },
        ),
      ),
    );
  }
}