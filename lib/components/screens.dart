import 'package:flutter/material.dart';
import 'package:mots_admin/theme/style.dart';

Widget screenTitle({
  String title,
  String tagline,
}) =>
  Container(
    alignment: Alignment.topLeft,
    margin: EdgeInsets.only(bottom: 2 * REM),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 0.5 * REM),
          child: Text(
            title.toUpperCase(),
            style: TextStyle(
              fontSize: REM * 2,
              color: theme.primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Text(
          tagline,
          style: TextStyle(
            color: theme.primaryColor,
          ),
        ),
      ],
    ),
  );
