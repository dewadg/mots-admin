
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:graphql/client.dart';
import 'package:mots_admin/storage/auth_storage.dart';

final HttpLink httpLink = HttpLink(
  uri: DotEnv().env['API_HOST'] + "/query",
);

final AuthLink authLink = AuthLink(
  getToken: () async {
    String accessToken = authStorage.getItem('accessToken').toString();

    return accessToken == null ? '' : 'Bearer $accessToken';
  }
);

final Link finalLink = authLink.concat(httpLink);

final GraphQLClient gqlClient = GraphQLClient(
  link: finalLink,
  cache: InMemoryCache(),
);