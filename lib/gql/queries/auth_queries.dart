const String CheckAdminQuery = r'''
  query WhoAmI {
    whoami {
      role {
        id
        name
      }
    }
  }
''';