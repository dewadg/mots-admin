const String AllReportsQueries = r'''
  query{
    reports{
      contractNumber,
      description,
      duration,
      id,
      images{
        fileName,
        mimeType,
        url
      },
      jobName,
      lat,
      lng,
      user {
        id
        username,
        personal {
          fullName
        }
      }
    }
  }
''';