const String AuthMutation = r'''
  mutation Authenticate($username: String, $password: String) {
    authenticate(username: $username, password: $password)
  }
''';