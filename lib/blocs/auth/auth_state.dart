import 'package:equatable/equatable.dart';

class AuthState extends Equatable {
  final String username;
  final String password;

  final bool authLoading;
  final bool authSuccess;
  final Exception authError;

  AuthState({this.username, this.password, this.authLoading, this.authSuccess, this.authError});

  factory AuthState.initial() => AuthState(
    username: '',
    password: '',
    authLoading: false,
    authSuccess: false,
    authError: null
  );

  @override
  List<Object> get props => [
    username,
    password,
    authLoading,
    authSuccess,
    authError
  ];

  @override
  String toString() {
    return '''
      {
        username: $username,
        password: $password,
        authLoading: $authLoading,
        authSuccess: $authSuccess,
        authError: $authError
      }
    ''';
  }

  AuthState copyWith({
    String username,
    String password,
    bool authLoading,
    bool authSuccess,
    Exception authError
  }) => AuthState(
    username: username ?? this.username,
    password: password ?? this.password,
    authLoading: authLoading ?? this.authLoading,
    authSuccess: authSuccess ?? this.authSuccess,
    authError: authError,
  );
} 