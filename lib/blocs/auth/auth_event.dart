import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class UsernameChanged extends AuthEvent {
  final String username;

  UsernameChanged({this.username});

  @override
  List<Object> get props => [username];
}

class PasswordChanged extends AuthEvent {
  final String password;

  PasswordChanged({this.password});

  @override
  List<Object> get props => [password];
}

class AuthenticatingStarted extends AuthEvent {
  @override
  List<Object> get props => [];
}

class AuthenticatingError extends AuthEvent {
  final Exception error;

  AuthenticatingError({this.error});

  @override
  List<Object> get props => [error];
}

class AuthenticatingSuccess extends AuthEvent {
  @override
  List<Object> get props => [];
}