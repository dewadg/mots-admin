import 'package:bloc/bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mots_admin/blocs/auth/bloc.dart';
import 'package:mots_admin/exceptions/auth_exception.dart';
import 'package:mots_admin/gql/mutations/auth_mutations.dart';
import 'package:mots_admin/gql/queries/auth_queries.dart';
import 'package:mots_admin/storage/auth_storage.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  GraphQLClient _gqlClient;

  AuthBloc(this._gqlClient);

  @override
  AuthState get initialState => AuthState.initial();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is UsernameChanged) {
      yield currentState.copyWith(
        username: event.username,
        authError: null
      );
    }

    if (event is PasswordChanged) {
      yield currentState.copyWith(
        password: event.password,
        authError: null
      );
    }

    if (event is AuthenticatingStarted) {
      yield currentState.copyWith(
        authLoading: true,
        authSuccess: null
      );

      await _handleAuthenticatingStarted(currentState.username, currentState.password);
    }

    if (event is AuthenticatingSuccess) {
      yield currentState.copyWith(
        authLoading: false,
        authSuccess: true,
        authError: null
      );
    }

    if (event is AuthenticatingError) {
      yield currentState.copyWith(
        authError: event.error,
        authLoading: false
      );
    }
  }

  _handleAuthenticatingStarted(String username, String password) async {
    try {
      final MutationOptions options = MutationOptions(
        document: AuthMutation,
        variables: <String, dynamic>{
          "username": username,
          "password": password,
        }
      );

      QueryResult result = await _gqlClient.mutate(options);

      if (result.hasErrors) {
        this.dispatch(AuthenticatingError(error: InvalidCredentialsException()));
        return;
      }

      String authenticateToken = result.data['authenticate'].toString();

      await authStorage.ready;
      await authStorage.setItem('accessToken', authenticateToken);

      if (!(await _isAdmin(authenticateToken))) {
        await authStorage.deleteItem('accessToken');
        this.dispatch(AuthenticatingError(error: NotAdminException()));
        return;
      };

      this.dispatch(AuthenticatingSuccess());
    } catch(_) {
      this.dispatch(AuthenticatingError(error: AuthenticationFailedException()));
    }
  }

  Future<bool> _isAdmin(String authenticate) async {
    try {
      final QueryOptions options = QueryOptions(
        document: CheckAdminQuery,
      );

      QueryResult result = await _gqlClient.query(options);

      if(result.hasErrors) {
        return false;
      }

      return result.data['whoami']['role']['id'] == 1;
    } catch(_) {
      return false;
    }
  }
}