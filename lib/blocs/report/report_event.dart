import 'package:equatable/equatable.dart';
import 'package:mots_admin/models/report.dart';

abstract class ReportEvent extends Equatable {
  ReportEvent();
}

class ReportsFetchStarted extends ReportEvent {
  @override
  List<Object> get props => [];
}

class ReportsFetchSuccess extends ReportEvent {
  final List<Report> reports;

  ReportsFetchSuccess({this.reports});
  
  @override
  List<Object> get props => [reports];
}

class ReportsFetchError extends ReportEvent {
  final Exception error;

  ReportsFetchError({this.error});

  @override
  List<Object> get props => [error];
}