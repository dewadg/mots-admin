import 'package:bloc/bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mots_admin/blocs/report/bloc.dart';
import 'package:mots_admin/exceptions/report_exception.dart';
import 'package:mots_admin/gql/queries/report_queries.dart';
import 'package:mots_admin/models/report.dart';

class ReportBloc extends Bloc<ReportEvent, ReportState> {
  final GraphQLClient _gqlClient;

  ReportBloc(this._gqlClient);
  
  @override
  ReportState get initialState => ReportState.initial();

  @override
  Stream<ReportState> mapEventToState(ReportEvent event) async* {
    if (event is ReportsFetchStarted) {
      yield currentState.copyWith(
        fetchingReportLoading: true,
        fetchingReportError: null,
      );

      await _handleReportsFetchStarted();
    }

    if (event is ReportsFetchSuccess) {
      yield currentState.copyWith(
        reports: event.reports,
        fetchingReportLoading: false,
        fetchingReportSuccess: true,
        fetchingReportError: null
      );
    }

    if (event is ReportsFetchError) {
      yield currentState.copyWith(
        fetchingReportLoading: false,
        fetchingReportError: event.error
      );
    }
  }

  _handleReportsFetchStarted() async {
    try {
      QueryOptions options = QueryOptions(
        document: AllReportsQueries
      );

      QueryResult result = await this._gqlClient.query(options);

      if (result.hasErrors) {
        this.dispatch(ReportsFetchError(error: FetchingDataFailedException()));
        return;
      }

      List<Report> reports = result.data['reports']
        .map<Report>((report) {
          if (report['lat'] == 0) report['lat'] = 0.0;
          if (report['lng'] == 0) report['lng'] = 0.0;
          return Report.fromMap(report);
        })
        .toList();

      this.dispatch(ReportsFetchSuccess(reports: reports));
    } catch(_) {
      print(_);
      this.dispatch(ReportsFetchError(error: FetchingDataFailedException()));
    }
  }
}