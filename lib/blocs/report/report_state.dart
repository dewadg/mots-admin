import 'package:equatable/equatable.dart';
import 'package:mots_admin/models/report.dart';

class ReportState extends Equatable {
  final bool fetchingReportLoading;
  final bool fetchingReportSuccess;
  final Exception fetchingReportError;
  final List<Report> reports;

  ReportState({
    this.reports,
    this.fetchingReportLoading,
    this.fetchingReportSuccess,
    this.fetchingReportError
  });

  factory ReportState.initial() =>
    ReportState(
      reports: [],
      fetchingReportLoading: false,
      fetchingReportSuccess: false,
      fetchingReportError: null
    );

  @override
  List<Object> get props => [
    reports,
    fetchingReportLoading,
    fetchingReportSuccess,
    fetchingReportError
  ];

  @override
  String toString() => '''
    {
      reports: $reports,
      fetchingReportLoading: $fetchingReportLoading,
      fetchingReportSuccess: $fetchingReportSuccess,
      fetchingReportError: $fetchingReportError
    }
  ''';

  ReportState copyWith({
    List<Report> reports,
    bool fetchingReportLoading,
    bool fetchingReportSuccess,
    Exception fetchingReportError
  }) => 
    ReportState(
      reports: reports ?? this.reports,
      fetchingReportLoading: fetchingReportLoading ?? this.fetchingReportLoading,
      fetchingReportSuccess: fetchingReportSuccess ?? this.fetchingReportSuccess,
      fetchingReportError: fetchingReportError
    );
}