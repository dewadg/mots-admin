import 'package:flutter/material.dart';

const double REM = 13;

ThemeData appTheme() {
  return ThemeData(
    primaryColor: Colors.indigo,
    accentColor: Colors.indigoAccent,
    buttonColor: Colors.indigo,
    primaryColorDark: Color(0xff4A4A4A)
  );
}

final theme = appTheme();
