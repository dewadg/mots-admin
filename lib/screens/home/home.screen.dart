import 'package:flutter/material.dart';
import 'package:mots_admin/screens/home/fragments/dashboard.fragment.dart';
import 'package:mots_admin/screens/home/fragments/report.fragment.dart';
import 'package:mots_admin/screens/home/fragments/usermanagement.fragment.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  var _currentScreen = 0;

  var _screens = [
    DashboardFragmentScreen(),
    ReportFragmentScreen(),
    UserManagementFragmentScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _screens[_currentScreen],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentScreen,
        onTap: (ind) {setState(() {_currentScreen = ind;});},
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Dashboard'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance_wallet),
            title: Text('Laporan')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('User Manager')
          ),
        ],
      ),
    );
  }
}