import 'package:flutter/material.dart';
import 'package:mots_admin/models/report.dart';
import 'package:mots_admin/screens/report_detail/report-detail.dart';
import 'package:mots_admin/theme/style.dart';

Widget reportCard(BuildContext context, Report report) =>
  Container(
    padding: EdgeInsets.all(REM),
    margin: EdgeInsets.only(bottom: REM),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.grey),
      borderRadius: BorderRadius.all(Radius.circular(0.5 * REM)),
    ),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.account_box,
          size: 38,
          color: Color(0xff3F5498),
        ),
        SizedBox(width: 8),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '09/08/2019',
                style: TextStyle(
                  fontSize: 14,
                  color: Color(0xff4A4A4A)
                ),
              ),
              SizedBox(height: 4),
              Text(
                report.contractNumber,
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xff4A4A4A),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Align(
            alignment: FractionalOffset.bottomLeft,
            child: Text(
              report.userFullName.isEmpty ? 'Nama pelapor' : report.userFullName,
              style: TextStyle(
                fontSize: 18,
                color: Color(0xff4A4A4A),
              ),
            ),
          ),
        ),
        PopupMenuButton<int>(
          itemBuilder: (context) => [
            PopupMenuItem(
              value: 1,
              child: Text("Detail Report"),
            ),
          ],
          onSelected: (value){
            switch(value) {
              case 1:
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ReportDetailScreen(
                      report: report,
                    )),
                );
                break;
            }
          },
        ),
      ],
    ),
  );