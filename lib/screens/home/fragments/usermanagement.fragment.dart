import 'package:flutter/material.dart';

class UserManagementFragmentScreen extends StatefulWidget {
  @override
  _UserManagementFragmentScreenState createState() => _UserManagementFragmentScreenState();
}

class _UserManagementFragmentScreenState extends State<UserManagementFragmentScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text('User Management fragment'),
    );
  }
}