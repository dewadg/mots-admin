import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mots_admin/blocs/report/bloc.dart';
import 'package:mots_admin/components/screens.dart';
import 'package:mots_admin/screens/home/components/report-card.dart';
import 'package:mots_admin/theme/style.dart';

class DashboardFragmentScreen extends StatelessWidget {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: _bodyContent(context),
    );
  }

  Widget _bodyContent(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final ReportBloc reportBloc = BlocProvider.of<ReportBloc>(context);

    reportBloc.dispatch(ReportsFetchStarted());

    return Container(
      margin: EdgeInsets.only(top: statusBarHeight),
      padding: EdgeInsets.all(REM),
      child: Column(
        children: <Widget>[
          screenTitle(
            title: 'Daftar Laporan',
            tagline: ''
          ),
          Expanded(
            child: Container(
              child: BlocBuilder<ReportBloc, ReportState>(
                builder: (context, state) {
                  if (state.fetchingReportLoading) {
                    return Text('Memuat data...');
                  }

                  if (state.fetchingReportError != null) {
                    return Text(state.fetchingReportError.toString());
                  }

                  List<Widget> children = state.reports
                    .map<Widget>((report) {
                      return reportCard(context, report);
                    })
                    .toList();

                  return RefreshIndicator(
                    key: _refreshIndicatorKey,
                    onRefresh: () async {
                      reportBloc.dispatch(ReportsFetchStarted());
                    },
                    child: ListView(
                      padding: EdgeInsets.only(top: 0),
                      children: children,
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}