import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mots_admin/models/report.dart';
import 'package:mots_admin/theme/style.dart';

class ReportFormDisplay extends StatefulWidget {
  final Report report;
  
  ReportFormDisplay(this.report);

  @override
  _ReportFormDisplayState createState() => _ReportFormDisplayState();
}

class _ReportFormDisplayState extends State<ReportFormDisplay> {

  static LatLng _fallbackLatLng;

  Geolocator _geolocator;

  @override
  void initState() {
    _fallbackLatLng = LatLng(widget.report.lat, widget.report.lng);
    _geolocator = Geolocator()..forceAndroidLocationManager;

    super.initState();
  }


  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        mapDisplay(screenHeight),
        Padding(
          padding: EdgeInsets.only(left: REM, top: REM, right: REM),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Color(0xffDADADA)),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  padding: EdgeInsets.only(top: 16, bottom: 16),
                  decoration: BoxDecoration(
                    color: Color(0xffDADADA),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0)
                    ),
                  ),
                  child: Text(
                    'Laporan OTS',
                    style: TextStyle(
                      fontSize: REM *1,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(0, 0, 0, 0.867782)
                    ),
                  ),          
                ),
                Padding(
                  padding: EdgeInsets.all(REM),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      textField(
                        icon: Icons.alarm_add,
                        value: 'jan, 10 2019'
                      ),
                      SizedBox(height: 8),
                      textField(
                        icon: Icons.work,
                        value: widget.report.jobName
                      ),
                      SizedBox(height: 8),
                      textField(
                        value: widget.report.contractNumber,
                        icon: Icons.person_outline,
                      ),
                      SizedBox(height: 8),
                      textField(
                        value: widget.report.duration.toString() + ' jam',
                        icon: Icons.alarm,
                      ),
                      Text(
                        'Deskripsi',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xff979797)
                        ),
                      ),
                      SizedBox(height: 8),
                      textField(
                        value: widget.report.description,
                      ),
                      imagePickerField(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(REM),
            child: Align(
            alignment: Alignment.centerRight,
            child: submitButton(
              onPressed: () {
              }
            )
          ),
        ),
      ],
    );
  }

  Widget mapDisplay(double screenHeight) {
    final Set<Marker> _markers = {};

    _markers.add(Marker(
      markerId: MarkerId(_fallbackLatLng.toString()),
      position: _fallbackLatLng,
      infoWindow: InfoWindow(
        title: 'Lokasi Laporan',
        snippet: widget.report.contractNumber,
      ),
      icon: BitmapDescriptor.defaultMarker,
    ));

    return Container(
      height: screenHeight / 3,
      child: GoogleMap(
          initialCameraPosition: CameraPosition(
            target: _fallbackLatLng,
            zoom: 15,
          ),
          markers: _markers,
          gestureRecognizers: Set()
            ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
            ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
            ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
            ..add(Factory<VerticalDragGestureRecognizer>(() => VerticalDragGestureRecognizer())),
        ),
    );
  }


  Widget textField({
    String value,
    IconData icon,
  }) {

    List<Widget> children = [
      SizedBox(width: icon != null ? REM : 0),
      Text(
        value,
        style: TextStyle(
          fontSize: 16,
          color: Color(0xff979797)
        ),
      )
    ];

    if (icon != null) children.insert(0, Icon(icon, color: Color(0xff979797)));

    return Padding(
        padding: EdgeInsets.only(bottom: REM),
        child: Container(
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            border: Border.all(width: 0.5, color: Color(0xff979797)),
            borderRadius: BorderRadius.all(Radius.circular(5))
          ),
          child: Row(
            children: children,
          ),
        ),
      );
  }
  
  Widget imagePickerField() {
    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Foto',
            style: TextStyle(
              fontSize: 16,
              color: Color(0xff979797),
            ),
          ),
          buildGridView()
        ],
      ),
    );
  }

  Widget buildGridView() {
    List<Widget> imagesWidget = widget.report.images
      .map((image) {
        return Container(
          height: 24,
          margin: EdgeInsets.all(4),
          child: Image.network(image.url, fit: BoxFit.cover,
            loadingBuilder:(BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
            if (loadingProgress == null) return child;
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          ),
        );
      })
      .toList();

    return GridView.count(
      crossAxisCount: 3,
      physics: ScrollPhysics(),
      shrinkWrap: true,
      children: imagesWidget,
    );
  }

  Widget submitButton({
    Function onPressed,
  }) =>
    Padding(
      padding: EdgeInsets.only(bottom: REM),
      child: RaisedButton(
        color: theme.primaryColor,
        child: Text(
          'Selesai'.toUpperCase(),
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        onPressed: onPressed,
      ),
    );
}