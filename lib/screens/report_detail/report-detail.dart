import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mots_admin/blocs/report/bloc.dart';
import 'package:mots_admin/models/report.dart';
import 'package:mots_admin/screens/report_detail/components/report-form.dart';
import 'package:mots_admin/theme/style.dart';

class ReportDetailScreen extends StatefulWidget {
  final Report report;
  
  ReportDetailScreen({ Key key, this.report }) : super(key: key);

  @override
  _ReportDetailScreenState createState() => _ReportDetailScreenState();
}

class _ReportDetailScreenState extends State<ReportDetailScreen> {
  
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Detil Laporan'),
      ),
      body: SafeArea(
        child: ReportFormDisplay(widget.report),
      ),
    );
  }
}
