import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mots_admin/blocs/auth/bloc.dart';
import 'package:mots_admin/components/screens.dart';
import 'package:mots_admin/theme/style.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  TextEditingController _usernameCtrl;
  TextEditingController _passwordCtrl;

  @override
  void initState() {
    _usernameCtrl = TextEditingController();
    _passwordCtrl = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    clearForm();
    super.dispose();
  }

  void clearForm() {
    this._usernameCtrl.clear();
    this._passwordCtrl.clear();
  }

  @override
  Widget build(BuildContext context) {
    final AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);

    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        final scaffold = Scaffold.of(context);

        if (!state.authLoading && state.authError != null) {
          scaffold.removeCurrentSnackBar();
          scaffold.showSnackBar(SnackBar(
            content: Text(state.authError.toString()),
            duration: Duration(seconds: 1),
          ));
        }

        if (!state.authLoading && state.authSuccess) {
          Navigator.pushNamedAndRemoveUntil(context, '/dashboard', (Route<dynamic> route) => false);
        }
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(REM),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                screenTitle(
                  title: 'Masuk ke MOTS Admin',
                  tagline: 'Masuk dan mulai bekerja',
                ),
                usernameField(authBloc, _usernameCtrl),
                passwordField(authBloc, _passwordCtrl),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: REM),
                      child: Text(
                        'Lupa Kata Sandi?',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: theme.primaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: double.infinity,
                  child: loginButton(authBloc),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget usernameField(AuthBloc authBloc, TextEditingController controller) =>
    BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.only(bottom: REM),
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Nama Pengguna',
            ),
            onChanged: (String value) {
              authBloc.dispatch(UsernameChanged(username: value));
            },
          ),
        );
      },
    );

  Widget passwordField(AuthBloc authBloc, TextEditingController controller) =>
    BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.only(bottom: REM),
          child: TextField(
            obscureText: true,
            controller: controller,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Kata Sandi',
            ),
            onChanged: (String value) {
              authBloc.dispatch(PasswordChanged(password: value));
            },
          ),
        );
      },
    );

  Widget loginButton(AuthBloc authBloc) =>
    BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        String label = 'Login';
        if (state.authLoading) label = 'Mohon tunggu...';

        return Padding(
          padding: EdgeInsets.only(bottom: REM),
          child: RaisedButton(
            color: theme.primaryColor,
            child: Text(
              label.toUpperCase(),
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            onPressed: () {
              authBloc.dispatch(AuthenticatingStarted());
            },
          ),
        );
      },
    );
}