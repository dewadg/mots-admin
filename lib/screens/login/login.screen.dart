import 'package:flutter/material.dart';
import 'package:mots_admin/screens/login/components/login-form.dart';
import 'package:mots_admin/storage/auth_storage.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          LoginForm()
        ],
      ),
    );
  }
}