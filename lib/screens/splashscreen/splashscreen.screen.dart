import 'package:flutter/material.dart';
import 'package:mots_admin/storage/auth_storage.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    _isLoggedIn(context).then((loggedIn) {
      if (loggedIn) Navigator.pushNamedAndRemoveUntil(context, '/dashboard', (Route<dynamic> route) => false);
      else Navigator.pushNamedAndRemoveUntil(context, '/login', (Route<dynamic> route) => false);
    });

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Mots Admin')
          ],
        ),
      ),
    );
  }
  
  Future<bool> _isLoggedIn(BuildContext context) async {
    await authStorage.ready;

    return authStorage.getItem('accessToken') != null;
  }
}