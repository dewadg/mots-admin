class FetchingDataFailedException implements Exception {
  @override
  String toString() => 'Terjadi kesalahan pada saat pengambilan data';
}