class InvalidCredentialsException implements Exception {
  @override
  String toString() => 'Nama pengguna/kata sandi salah';
}

class AuthenticationFailedException implements Exception {
  @override
  String toString() => 'Terjadi kesalahan pada proses otentikasi';
}

class NotAdminException implements Exception {
  @override
  String toString() => 'Hanya admin yang diperbolehkan untuk login';
}